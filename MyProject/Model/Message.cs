﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class Message
    {
        public static List<Message> Messages = new List<Message>();

        public Guid Id;
        public User Author;
        public List<User> Participants = new List<User>();
        public Guid ChatId;
        public DateTime Time;
        public string Content;

        public Message(User author ,List<User> owners, string content)
        {
            Id = Guid.NewGuid();
            Participants = owners;
            Content = content;
            Author = author;

            foreach(var chat in Chat.Chats)
            {
                if (chat.Participants == Participants)
                {
                    ChatId = chat.Id;
                    Messages.Add(this);//added
                    return;//break;
                }
            }

            Chat generateChat = new Chat(owners);
            ChatId = generateChat.Id;

            Messages.Add(this);
        }
    }
}
