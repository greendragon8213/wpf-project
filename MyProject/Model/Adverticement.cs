﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class Adverticement
    {
        public static List<Adverticement> Adverticements = new List<Adverticement>();
        public Guid Id;
        private string _content;
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }
        private string _company;
        public string Company
        {
            get { return _company; }
            set { _company = value;  } //ToDo delete it!
        }
        private Guid _employerId;
        public Guid EmployerId
        {
            get { return _employerId; }
            set { _employerId = value; }
        }
        private State _state;
        public State State
        {
            get { return _state; }
            set { _state = value; }
        }
        private Employer _author;
        public Employer Author
        {
            get
            {
                foreach (Employer em in Employer.Employers)
                    if (em.Id == EmployerId)
                        return em;
                return null;
            }
            set
            {
                _author = value;
            }
            
        }

        public Adverticement()
        {

        }

        public Adverticement( Employer empl, string cont=null)
        {
            Content = cont;
            Id = Guid.NewGuid();
            EmployerId = empl.Id;
            Author = empl;
            _company = empl.CompanyName;
            Date = DateTime.Now;
            State = State.Actual;
            //Adverticements.Add(this);
        }
    }
}
