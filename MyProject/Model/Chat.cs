﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class Chat
    {
        public static List<Chat> Chats = new List<Chat>();

        public Guid Id;
        public List<User> Participants = new List<User>();

        public List<Message> Messages
        {
            get
            {
                var res = new List<Message>();
                foreach (var mes in Message.Messages)
                {
                    if (mes.ChatId == Id)
                    {
                        res.Add(mes);
                    }
                }
                return res;
            }
        }

        public Chat(List<User> participants)
        {
            Id = Guid.NewGuid();
            Participants.AddRange(participants);
            Chats.Add(this);
        }

    }
}
