﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class UserInChat
    {
        //static public Dict<UserInChat> Chat2Users = new List<UserInChat>();
        static public List<UserInChat> Chat2Users = new List<UserInChat>();

        private Guid _chatId;
        public Chat Chat
        {
            get
            {
                foreach (var chat in Chat.Chats)
                {
                    if (chat.Id == _chatId)
                    {
                        return chat;
                    }
                }
                return null;
            }
            set { _chatId = value.Id; }
        }

        private Guid _userId;
        public User User
        {
            get
            {
                foreach (var user in User.Users)
                {
                    if (user.Id == _userId)
                    {
                        return user;
                    }
                }
                return null;
            }
            set { _userId = value.Id; }
        }
        
        public UserInChat()
        {
            Chat2Users.Add(this);
        }
    }
}
