﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public enum Roles { User, Moderator, Admin };
    public enum Positions { Candidate, Employer };
    public enum State { Actual, Unactual};
    public enum ApprovingStatus {  Unapproved, Approved};
    public class User
    {
        public static List<User> Users = new List<User>();

        public string UserName;
        public string Password;
        public Guid Id;
        public Roles Role;
        public Positions Position ;

        private ApprovingStatus _state;
        public ApprovingStatus State
        {
            get { return _state; }
            set { _state = value; }
        }

        private List<Chat> chats;
        public List<Chat> Chats
        {
            get
            {
                var res = new List<Chat>();
                foreach (var chat in Chat.Chats)
                {
                    if (chat.Participants.Contains(this))
                    {
                        res.Add(chat);
                    }
                }
                return res;
            }
            set { chats = value; }
        }

        public List<UserInChat> Chat2Users
        {
            get
            {
                var res = new List<UserInChat>();
                foreach (var item in UserInChat.Chat2Users)
                {
                    if (item.User == this)
                    {
                        res.Add(item);
                    }
                }
                return res;
            }
        }

        public User(Positions pos)
        {
            State = ApprovingStatus.Unapproved;
            Position = pos;
            Id = Guid.NewGuid();
            Users.Add(this);
        }
    }
}
