﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class Candidate: User
    {
        public static List<Candidate> Candidates = new List<Candidate>();

        private Guid _id;

        public new Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private Guid _userId;

        public Guid UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        private string _experience;
        public string Experience
        {
            get { return _experience; }
            set { _experience = value; }
        }
        private string _skills;
        public string Skills
        {
            get { return _skills; }
            set { _skills = value; }
        }
        private int? _salary;
        public int? Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }
        private string _location;
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        
        public Candidate():base(Positions.Candidate)
        {
            UserId = base.Id;
            Candidates.Add(this);
        }

        public static void SetTestData()
        {
            Candidate c1 = new Candidate();
            Candidate c2 = new Candidate();
            Candidate c3 = new Candidate();
            Candidate c4 = new Candidate();
            Candidate c5 = new Candidate();
            Candidate c6 = new Candidate();

            c1.Experience = "Over 30 years' experience in the legal profession, with a strong background in civil litigation, excellent interpersonal skills and the ability to communicate effectively.";
            c2.Experience = "10 years of experience with a strong environmental awareness, excellent writing skills, and the ability to find innovative solutions.";
            c3.Experience = "Passionate intercultural expert with a blend of experience in mediation, teaching, writing, radio producing, public speaking and management in international and diverse settings.";
            c4.Experience = "More than 20 years as a knowledgeable and effective psychologist working with individuals, groups, and facilities, with particular emphasis on geriatrics and the multiple psychopathologies within that population.";
            c5.Experience = "More than 10 years' experience in training development and delivery, motivation and team building/leadership, general and technical project management, item marketing and management, negotiation, and mediation.";
            c6.Experience = "Nine years' experience developing, implementing, and managing complex projects within time and budgetary constraints.";

            c1.Location = "Lviv";
            c2.Location = "Kyiv";
            c3.Location = "Ternopil";
            c4.Location = "Kharkiv";
            c5.Location = "Kyiv";
            c6.Location = "Dnipropetrovsk";

            c1.Salary = 2500;
            c2.Salary = 1000;
            c3.Salary = 89;

            c1.Skills = "I'm very fine developer. I have a lot of experience in qqq ooo ddd progr.";
            c2.Skills = "More than six years of senior software engineering experience, with strong analytical skills and a broad range of computer expertise.";
            c3.Skills = "Strong communication, interpersonal, and presentation skills.";
            c4.Skills = "Strong leadership skills; able to prioritize, delegate tasks, and make sound decisions quickly while maintaining a focus on the bottom line.";
            c5.Skills = "masm dasm qqq";
            c6.Skills = "Computer skills include: MS Project, Excel, Word; AutoPlan; Applix Word, Graphics, Spreadsheet; Powerpoint, FrameMaker, Lotus Notes, NetObjects Fusion, Dreamweaver and SAP (MM, SD), SunOS. Familiar with HTML and SQL database queries.";

            c1.State = ApprovingStatus.Approved;
            c2.State = ApprovingStatus.Unapproved;
            c3.State = ApprovingStatus.Unapproved;
            c4.State = ApprovingStatus.Approved;
            c5.State = ApprovingStatus.Unapproved;
            c6.State = ApprovingStatus.Approved;
        }
    }
}
