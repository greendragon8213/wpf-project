﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkSearch.classes
{
    public class Employer : User
    {
        public static List<Employer> Employers = new List<Employer>();

        private Guid _id;
        public new Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private string _secondName;
        public string SecondName
        {
            get { return _secondName; }
            set { _secondName = value;}
        }
        private string _companyName;
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }
        
        public Guid UserId;

        private List<Adverticement> _ads = new List<Adverticement>();
        public List<Adverticement> Ads
        {
            get { return _ads; }
            set { _ads = value; }
        }
       
        public Employer(): base(Positions.Employer)
        {
            UserId = base.Id;
            Employers.Add(this);
        }

        public string EmployerFullName
        {
            get
            {
                return FirstName + " " + SecondName;
            }
        }

        ////public static void SetTestData()
        ////{
        ////    Employer e1 = new Employer();
        ////    e1.FirstName = "Tony";
        ////    e1.SecondName = "sn";
        ////    e1.Ads.Add(new Adverticement("Looking for fdfdfdfd", e1));
        ////    e1.CompanyName = "EEEEEEEEEEEE";
        ////    Employer e2 = new Employer();
        ////    e2.FirstName = "Mony";
        ////    Employer e3 = new Employer();
        ////    e3.FirstName = "Tom";
        ////    Employer e4 = new Employer();
        ////    e4.FirstName = "Susy";
        ////    e4.CompanyName = "Microsoft";
        ////    e4.Ads.Add( new Adverticement("qqqqqqqq",e4));
        ////    e4.Ads.Add(new Adverticement("looking for c# dev", e4));
        ////    e4.Ads.Add(new Adverticement("test3", e4));
        ////    Employer e5 = new Employer();
        ////    e5.FirstName = "Helga";
        ////}

    }
}
