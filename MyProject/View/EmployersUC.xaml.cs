﻿using MyProject.Data_Layer;
using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkSearch.classes;

namespace MyProject.View
{
    /// <summary>
    /// Interaction logic for EmployersUC.xaml
    /// </summary>
    public partial class EmployersUC : UserControl
    {
        EmployersUCVM _viewModel;
        public EmployersUC()
        {
            InitializeComponent();

            ////if (Employer.Employers.Count == 0)//Set test data
            ////    Employer.SetTestData();
            ////_viewModel = new EmployersUCVM(Employer.Employers);

            _viewModel = new EmployersUCVM(EmployersManager.GetAllEmployerFromDB());
            DataContext = _viewModel;
        }

        private void EditInfoBtn_Click(object sender, RoutedEventArgs e)
        {
            EmployerEditWindow window = new EmployerEditWindow(new EmployerEditWindowVM(_viewModel, (EmployerVM)EmployerListBox.SelectedItem));
            window.ShowDialog();
        }
    }
}
