﻿using MyProject.Data_Layer;
using MyProject.View;
using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkSearch.classes;

namespace MyProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowVM _viewModel;
        public MainWindow()
        {
            InitializeComponent();

            _viewModel = new MainWindowVM();
            DataContext = _viewModel;
        }

        private void ButtonCandidates_Click(object sender, RoutedEventArgs e)
        {
            ContentPanel.Children.Clear();
            ContentPanel.Children.Add(new CandidatesUC());
        }

        private void ButtonEmployers_Click(object sender, RoutedEventArgs e)
        {
            ContentPanel.Children.Clear();
            ContentPanel.Children.Add(new EmployersUC());
        }

        private void ButtonAdverticements_Click(object sender, RoutedEventArgs e)
        {
            ContentPanel.Children.Clear();
            ContentPanel.Children.Add(new AdverticementsUC(AdverticementsManager.GetAllAdverticementFromDB()));
        }

    }
}
