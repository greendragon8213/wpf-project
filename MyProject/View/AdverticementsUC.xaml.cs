﻿using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkSearch.classes;

namespace MyProject.View
{
    /// <summary>
    /// Interaction logic for AdverticementsUC.xaml
    /// </summary>
    public partial class AdverticementsUC : UserControl
    {
        AdverticementsUCVM _viewModel;
        public AdverticementsUC(List<Adverticement> adsList)
        {
            InitializeComponent();

            ////if (adsList.Count == 0)
            ////    return;

            ////if (Employer.Employers.Count == 0)//Set test data
            ////    Employer.SetTestData();

            _viewModel = new AdverticementsUCVM(adsList);
            DataContext = _viewModel;
        }

        private void EditInfoBtn_Click(object sender, RoutedEventArgs e)
        {
            AdverticementEditWindow window = new AdverticementEditWindow(new AdverticementEditWindowVM(_viewModel, (AdverticementVM)AdverticementListBox.SelectedItem));
            window.ShowDialog();
        }
    }
}
