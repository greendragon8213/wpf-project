﻿using MyProject.Data_Layer;
using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorkSearch.classes;

namespace MyProject.View
{
    /// <summary>
    /// Interaction logic for CandidateUC.xaml
    /// </summary>
    public partial class CandidatesUC : UserControl
    {
        CandidatesUCVM _viewModel;
        
        public CandidatesUC()
        {
            InitializeComponent();

            _viewModel = new CandidatesUCVM(CandidatesManager.GetAllCandidateFromDB());
            DataContext = _viewModel;
        }

        private void EditInfoBtn_Click(object sender, RoutedEventArgs e)
        {
            CandidateEditWindow window = new CandidateEditWindow(new CandidateEditWindowVM(_viewModel, (CandidateVM)CandidateListBox.SelectedItem));
            window.ShowDialog();
        }
    }
}
