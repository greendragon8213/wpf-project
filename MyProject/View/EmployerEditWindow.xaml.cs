﻿using MyProject.Data_Layer;
using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorkSearch.classes;

namespace MyProject.View
{
    /// <summary>
    /// Interaction logic for EmployerEditWindow.xaml
    /// </summary>
    public partial class EmployerEditWindow : Window
    {
        private EmployerEditWindowVM _viewModel;
        public EmployerEditWindow(EmployerEditWindowVM viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;

            ContentPanel.Children.Clear();
            ContentPanel.Children.Add(new AdverticementsUC(AdverticementsManager.GetAdsByEmployerId(viewModel.EmployerId)));
            ////ContentPanel.Children.Add(new AdverticementsUC(Adverticement.Adverticements.Select(ad => ad).Where(ad => ad.EmployerId == viewModel.EmployerId).ToList()));
        }

        private void ApplyBtn_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ButtonOKClick();
            Close();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
