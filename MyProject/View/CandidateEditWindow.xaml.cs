﻿using MyProject.Data_Layer;
using MyProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyProject.View
{
    /// <summary>
    /// Interaction logic for CandidateEditWindow.xaml
    /// </summary>
    public partial class CandidateEditWindow : Window
    {
        private CandidateEditWindowVM _viewModel;
        public CandidateEditWindow(CandidateEditWindowVM viewModel)
        {
            InitializeComponent();

            _viewModel = viewModel;
            DataContext = _viewModel;
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ApplyBtn_Click(object sender, RoutedEventArgs e)
        {
            _viewModel.ButtonOKClick();
            Close();
        }
    }
}
