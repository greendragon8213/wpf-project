namespace MyProject.Data_Layer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    public partial class Users
    {
        private DataRow _row;

        public DataRow Row
        {
            get { return _row; }
            set
            {
                if (_row == null)
                    _row = value;
            }
        }
        #region Users
        public Users(DataRow row)
        {
            ////Candidates = new HashSet<Candidates>();
            _row = row;
        }

        public Guid Id { get; set; }

        [Required]
        [StringLength(64)]
        public string UserName { get; set; }

        [Required]
        [StringLength(64)]
        public string Password { get; set; }

        public int Role { get; set; }

        public int Position { get; set; }

        public int ApprovingStatus { get; set; }
        
        #endregion
    }
}
