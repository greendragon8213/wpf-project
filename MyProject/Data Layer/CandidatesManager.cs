﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.Data_Layer
{
    class CandidatesManager
    {
        private DataSet _dataSet = new DataSet();
        private SqlDataAdapter _candidatesAdapter;

        public static void CommitDataToDB(Candidate cand)
        {
            CandidatesManager _managerC = new CandidatesManager();
            UsersManager _managerU = new UsersManager();

            _managerU.GetUsersByAdapter();
            _managerC.GetCandidatesByAdapter();

            Candidates Candidates = new Candidates(null)
            {
                Id = cand.Id,
                UserId = cand.UserId,
                Skills = cand.Skills,
                Salary = cand.Salary,
                Location = cand.Location,
                Experience = cand.Experience
            };

            Users Users = new Users(null)
            {
                Id = cand.UserId,
                ApprovingStatus = Convert.ToInt32(cand.State),
                UserName = cand.UserName,
                Password = cand.Password
            };

            _managerU.Save(Users);

            _managerC.Save(Candidates);
        }

        public static List<Candidate> GetAllCandidateFromDB()
        {
            ////if (Candidate.Candidates.Count == 0)//Set test data
            ////    Candidate.SetTestData();

            CandidatesManager _managerC;
            UsersManager _managerU;

            _managerC = new CandidatesManager();
            _managerU = new UsersManager();

            var candidates = _managerC.GetCandidatesByAdapter();
            var users = _managerU.GetUsersByAdapter();

            List<Candidate> Candidates = new List<Candidate>();
            foreach (var c in candidates)
            {
                Candidates.Add(new Candidate()
                {
                    Id = c.Id,
                    UserId = c.UserId,
                    Skills = c.Skills,
                    Salary = c.Salary,
                    Location = c.Location,
                    Experience = c.Experience,

                    State = (ApprovingStatus)users.Where(o => o.Id == c.UserId).Select(o => o.ApprovingStatus).FirstOrDefault(),
                    UserName = users.Where(o => o.Id == c.UserId).Select(o => o.UserName).FirstOrDefault(),
                    Password = users.Where(o => o.Id == c.UserId).Select(o => o.Password).FirstOrDefault().ToString()

                });
            }

            return Candidates;
        }

        public Candidates[] GetCandidates()
        {
            List<Candidates> candidatesList = new List<Candidates>();

            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);

            string selectCandidatesSQL = "SELECT [Id], [UserName], [Password],[Role],[Position],[ApprovingStatus] FROM [dbo].[Users]";
            SqlCommand selectCandidatesCommand = new SqlCommand(selectCandidatesSQL, connection);

            connection.Open();
            using (var dataReader = selectCandidatesCommand.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    Candidates candidate = new Candidates(null);

                    candidate.Id = (Guid)(dataReader["Id"]);
                    candidate.UserId = (Guid)(dataReader["UserId"]);
                    candidate.Experience = dataReader["Experience"].ToString();
                    candidate.Skills = dataReader["Skills"].ToString();
                    candidate.Salary = Convert.ToInt32(dataReader["Salary"]);
                    candidate.Location = (dataReader["Location"].ToString());

                    candidatesList.Add(candidate);
                }
            }

            connection.Close();

            return candidatesList.ToArray();
        }

        public void Save(Candidates item)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            _dataSet.Tables["Candidates"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Candidates"].Columns[0] };
            item.Row = _dataSet.Tables["Candidates"].Rows.Find(item.Id);
            if (item.Row == null)
            {
                item.Row = _dataSet.Tables["Candidates"].NewRow();
                _dataSet.Tables["Candidates"].Rows.Add(item.Row);
            }
            item.Row["Id"] = item.Id;
            item.Row["UserId"] = item.UserId;
            item.Row["Experience"] = item.Experience;
            item.Row["Skills"] = item.Skills;
            item.Row["Salary"] = item.Salary;
            item.Row["Location"] = item.Location;

            GetCandidatesDataAdapter().Update(_dataSet, "Candidates");
        }

        public void Save(Candidates[] candidates)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            foreach (var item in candidates)
            {
                ////var row = _dataSet.Tables["Product"].Rows.Find(product.ProductID);
                ////if (row != null)
                if (item.Row != null)
                {
                        item.Row["UserId"] = item.UserId;
                        item.Row["Experience"] = item.Experience;
                        item.Row["Skills"] = item.Skills;
                        item.Row["Salary"] = item.Salary;
                        item.Row["Location"] = item.Location;
                }
                else
                {
                    ////CostTable.Rows[0].Table.PrimaryKey = new DataColumn[] { CostTable.Columns[0] };
                    _dataSet.Tables["Candidates"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Candidates"].Columns[0] };
                    item.Row = _dataSet.Tables["Candidates"].Rows.Find(item.Id);
                    if(item.Row==null)
                    { 
                        item.Row = _dataSet.Tables["Candidates"].NewRow();
                        _dataSet.Tables["Candidates"].Rows.Add(item.Row);
                    }
                    item.Row["Id"] = item.Id;
                    item.Row["UserId"] = item.UserId;
                    item.Row["Experience"] = item.Experience;
                    item.Row["Skills"] = item.Skills;
                    item.Row["Salary"] = item.Salary;
                    item.Row["Location"] = item.Location;
                }
            }

            GetCandidatesDataAdapter().Update(_dataSet, "Candidates");
        }

        public Candidates[] GetCandidatesByAdapter()
        {
            List<Candidates> candidatesList = new List<Candidates>();

            SqlDataAdapter candidatesAdapter = GetCandidatesDataAdapter();

            candidatesAdapter.Fill(_dataSet, "Candidates");

            foreach (DataRow row in _dataSet.Tables["Candidates"].Rows)
            {
                Candidates candidate = new Candidates(row);

                candidate.Id = (Guid)(row["Id"]);
                candidate.UserId = (Guid)(row["UserId"]);
                candidate.Experience = row["Experience"].ToString();
                candidate.Skills = row["Skills"].ToString();
                candidate.Salary = Convert.ToInt32(row["Salary"]);
                candidate.Location = (row["Location"].ToString());

                candidatesList.Add(candidate);
            }

            return candidatesList.ToArray();
        }

        private SqlDataAdapter GetCandidatesDataAdapter()
        {
            if (_candidatesAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectCandidatesSQL = "SELECT * FROM [dbo].[Candidates]";
                SqlCommand selectUsersCommand = new SqlCommand(selectCandidatesSQL, connection);

                _candidatesAdapter = new SqlDataAdapter(selectCandidatesSQL, connection);

                CreateUpdateCommand(_candidatesAdapter);
            }
            return _candidatesAdapter;
        }

        private SqlDataAdapter GetCandidateDataAdapter(Guid id)
        {
            if (_candidatesAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectCandidatesSQL = "SELECT * FROM [dbo].[Candidates] WHERE Id =" + id.ToString();
                SqlCommand selectUsersCommand = new SqlCommand(selectCandidatesSQL, connection);

                _candidatesAdapter = new SqlDataAdapter(selectCandidatesSQL, connection);

                CreateUpdateCommand(_candidatesAdapter);
            }
            return _candidatesAdapter;
        }

        private void CreateUpdateCommand(SqlDataAdapter adapter)
        {
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
            adapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            adapter.InsertCommand = commandBuilder.GetInsertCommand();
            ////adapter.DeleteCommand = commandBuilder.GetDeleteCommand();
        }

        private string GetConnectionString()
        {
            return @"Data Source=(localdb)\v11.0;Initial Catalog=MyDb;Integrated Security=True";
        }
    }
}
