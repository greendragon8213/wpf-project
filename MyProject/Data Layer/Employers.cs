namespace MyProject.Data_Layer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    public partial class Employers
    {
        private DataRow _row;

        public DataRow Row
        {
            get { return _row; }
            set
            {
                if (_row == null)
                    _row = value;
            }
        }
        public Employers(DataRow row)
        {
            _row = row;
            ////Adverticements = new HashSet<Adverticement>();ToDo
        }

        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        [StringLength(15)]
        public string FirstName { get; set; }

        [StringLength(15)]
        public string SecondName { get; set; }

        [StringLength(30)]
        public string Company { get; set; }


        public virtual ICollection<Adverticements> Adverticements { get; set; }

        public virtual Users User { get; set; }
    }
}
