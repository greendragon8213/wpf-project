namespace MyProject.Data_Layer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    public partial class Candidates
    {
        private DataRow _row;

        public DataRow Row
        {
            get { return _row; }
            set
            {
                if (_row == null)
                    _row = value;
            }
        }

        public Candidates(DataRow row)
        {
            _row = row;
            //ToDo add UserId
        }

        ////public Candidates(Guid id, Guid userId)
        ////{
        ////    _row["UserId"] = userId;
        ////    _row["Id"] = id;
        ////    Id = id;
        ////    UserId = userId;
            
        ////    //ToDo add UserId
        ////}

        #region candidates
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public string Experience { get; set; }

        public string Skills { get; set; }

        public int? Salary { get; set; }

        [StringLength(1024)]
        public string Location { get; set; }
        #endregion
    }
}
