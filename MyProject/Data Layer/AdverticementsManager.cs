﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.Data_Layer
{
    class AdverticementsManager
    {
        private DataSet _dataSet = new DataSet();
        private SqlDataAdapter _adverticementsAdapter;

        public static void CommitDataToDB(Adverticement ad)
        {
            AdverticementsManager _managerA = new AdverticementsManager();
            _managerA.GetAdverticementsByAdapter();

            Adverticements Adverticements = new Adverticements(null)
            {
                Id = ad.Id,
                AuthorId = ad.EmployerId,
                Content = ad.Content,
                CreationDate = ad.Date,
                Company = ad.Company,
                State = (int)ad.State
            };

            _managerA.Save(Adverticements);
        }
        public static List<Adverticement> GetAllAdverticementFromDB()
        {
            AdverticementsManager _managerA;

            _managerA = new AdverticementsManager();

            var ads = _managerA.GetAdverticementsByAdapter();

            List<Adverticement> Adverticements = new List<Adverticement>();
            foreach (var a in ads)
            {
                Adverticements.Add(new Adverticement(EmployersManager.GetAllEmployerFromDB().Where(o => o.Id == a.AuthorId).Select(o => o).FirstOrDefault())
                {
                    Id = a.Id,
                    //EmployerId = a.AuthorId,
                    Content = a.Content,
                    Date = a.CreationDate,
                    State = (State)a.State                    
                });
            }

            return Adverticements;
        }
        public static List<Adverticement> GetAdsByEmployerId(Guid emplId)
        {
            List<Adverticement> Ads = new List<Adverticement>();

            AdverticementsManager _managerA = new AdverticementsManager();
            var ads = _managerA.GetAdverticementsByAdapter();

            foreach (var a in ads)
            {
                if (a.AuthorId == emplId)
                {
                    Ads.Add(new Adverticement()//ToDo it!!!
                    {
                        Content = a.Content,
                        Date = a.CreationDate,
                        State = (State)a.State,
                        Id = a.Id,
                        EmployerId = emplId,
                        Company = a.Company //ToDo it
                    });
                }
            }

            return Ads;
        }

        public Adverticements[] GetAdverticements()
        {
            List<Adverticements> adverticementsList = new List<Adverticements>();

            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);

            string selectAdverticementsSQL = 
                "SELECT [Id], [AuthorId], [Content],[CreationDate],[Company],[State] FROM [dbo].[Adverticements]";
            SqlCommand selectAdverticementsCommand = new SqlCommand(selectAdverticementsSQL, connection);

            connection.Open();
            using (var dataReader = selectAdverticementsCommand.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    Adverticements adverticement = new Adverticements(null);

                    adverticement.Id = (Guid)(dataReader["Id"]);
                    adverticement.AuthorId = (Guid)(dataReader["AuthorId"]);
                    adverticement.Content = dataReader["Content"].ToString();
                    adverticement.CreationDate = (DateTime)dataReader["CreationDate"];
                    adverticement.Company = (dataReader["Company"]).ToString();
                    adverticement.State = Convert.ToInt32(dataReader["State"]);

                    adverticementsList.Add(adverticement);
                }
            }

            connection.Close();

            return adverticementsList.ToArray();
        }

        public void Save(Adverticements item)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            _dataSet.Tables["Adverticements"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Adverticements"].Columns[0] };
            item.Row = _dataSet.Tables["Adverticements"].Rows.Find(item.Id);
            if (item.Row == null)
            {
                item.Row = _dataSet.Tables["Adverticements"].NewRow();
                _dataSet.Tables["Adverticements"].Rows.Add(item.Row);
            }
            item.Row["Id"] = item.Id;
            item.Row["AuthorId"] = item.AuthorId;
            item.Row["Company"] = item.Company;
            item.Row["State"] = item.State;
            item.Row["CreationDate"] = item.CreationDate;
            item.Row["Content"] = item.Content;

            GetAdverticementsDataAdapter().Update(_dataSet, "Adverticements");
        }

        public void Save(Adverticements[] adverticements)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            foreach (var item in adverticements)
            {
                ////var row = _dataSet.Tables["Product"].Rows.Find(product.ProductID);
                ////if (row != null)
                if (item.Row != null)
                {
                    item.Row["Id"] = item.Id;
                    item.Row["AuthorId"] = item.AuthorId;
                    item.Row["Company"] = item.Company;
                    item.Row["State"] = item.State;
                    item.Row["CreationDate"] = item.CreationDate;
                    item.Row["Content"] = item.Content;
                }
                else
                {
                    ////CostTable.Rows[0].Table.PrimaryKey = new DataColumn[] { CostTable.Columns[0] };
                    _dataSet.Tables["Adverticements"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Adverticements"].Columns[0] };
                    item.Row = _dataSet.Tables["Adverticements"].Rows.Find(item.Id);
                    if(item.Row==null)
                    {
                        item.Row = _dataSet.Tables["Adverticements"].NewRow();
                        _dataSet.Tables["Adverticements"].Rows.Add(item.Row);
                    }
                    item.Row["Id"] = item.Id;
                    item.Row["AuthorId"] = item.AuthorId;
                    item.Row["Company"] = item.Company;
                    item.Row["State"] = item.State;
                    item.Row["CreationDate"] = item.CreationDate;
                    item.Row["Content"] = item.Content;
                }
            }

            GetAdverticementsDataAdapter().Update(_dataSet, "Adverticements");
        }

        public Adverticements[] GetAdverticementsByAdapter()
        {
            List<Adverticements> adverticementsList = new List<Adverticements>();

            SqlDataAdapter adverticementsAdapter = GetAdverticementsDataAdapter();

            adverticementsAdapter.Fill(_dataSet, "Adverticements");

            foreach (DataRow row in _dataSet.Tables["Adverticements"].Rows)
            {
                Adverticements adverticement = new Adverticements(row);

                adverticement.Id = (Guid)(row["Id"]);
                adverticement.AuthorId = (Guid)(row["AuthorId"]);
                adverticement.Content = row["Content"].ToString();
                adverticement.State = Convert.ToInt32(row["State"]);
                adverticement.CreationDate = (DateTime)(row["CreationDate"]);
                adverticement.Company = (row["Company"].ToString());

                adverticementsList.Add(adverticement);
            }

            return adverticementsList.ToArray();
        }

        private SqlDataAdapter GetAdverticementsDataAdapter()
        {
            if (_adverticementsAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectAdverticementsSQL = "SELECT * FROM [dbo].[Adverticements]";
                SqlCommand selectAdverticementsCommand = new SqlCommand(selectAdverticementsSQL, connection);

                _adverticementsAdapter = new SqlDataAdapter(selectAdverticementsSQL, connection);

                CreateUpdateCommand(_adverticementsAdapter);
            }
            return _adverticementsAdapter;
        }

        private SqlDataAdapter GetAdverticementsDataAdapter(Guid id)
        {
            if (_adverticementsAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectAdverticementsSQL = "SELECT * FROM [dbo].[Adverticements] WHERE Id =" + id.ToString();
                SqlCommand selectAdverticementsCommand = new SqlCommand(selectAdverticementsSQL, connection);

                _adverticementsAdapter = new SqlDataAdapter(selectAdverticementsSQL, connection);

                CreateUpdateCommand(_adverticementsAdapter);
            }
            return _adverticementsAdapter;
        }

        private void CreateUpdateCommand(SqlDataAdapter adapter)
        {
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
            adapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            adapter.InsertCommand = commandBuilder.GetInsertCommand();
            ////adapter.DeleteCommand = commandBuilder.GetDeleteCommand();
        }

        private string GetConnectionString()
        {
            return @"Data Source=(localdb)\v11.0;Initial Catalog=MyDb;Integrated Security=True";
        }
    }
}
