﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.Data_Layer
{
    class EmployersManager
    {
        private DataSet _dataSet = new DataSet();
        private SqlDataAdapter _employersAdapter;

        public static void CommitDataToDB(Employer empl)
        {
            EmployersManager _managerE = new EmployersManager();
            UsersManager _managerU = new UsersManager();

            _managerU.GetUsersByAdapter();
            _managerE.GetEmployersByAdapter();

            Employers Employers = new Employers(null)
            {
                Id = empl.Id,
                UserId = empl.UserId,
                FirstName = empl.FirstName,
                SecondName = empl.SecondName,
                Company = empl.CompanyName,
            };

            Users Users = new Users(null)
            {
                Id = empl.UserId,
                ApprovingStatus = Convert.ToInt32(empl.State),
                UserName = empl.UserName,
                Password = empl.Password
            };

            _managerU.Save(Users);

            _managerE.Save(Employers);
        }
        public static List<Employer> GetAllEmployerFromDB()
        {
            EmployersManager _managerE;
            UsersManager _managerU;

            _managerE = new EmployersManager();
            _managerU = new UsersManager();

            var employers = _managerE.GetEmployersByAdapter();
            var users = _managerU.GetUsersByAdapter();

            List<Employer> Employers = new List<Employer>();
            foreach (var c in employers)
            {
                Employers.Add(new Employer()
                {
                    Id = c.Id,
                    UserId = c.UserId,
                    FirstName = c.FirstName,
                    SecondName = c.SecondName,
                    CompanyName = c.Company,

                    Ads = AdverticementsManager.GetAdsByEmployerId(c.Id),

                    State = (ApprovingStatus)users.Where(o => o.Id == c.UserId).Select(o => o.ApprovingStatus).FirstOrDefault(),
                    UserName = users.Where(o => o.Id == c.UserId).Select(o => o.UserName).FirstOrDefault(),
                    Password = users.Where(o => o.Id == c.UserId).Select(o => o.Password).FirstOrDefault().ToString()

                });
            }

            return Employers;
        }

        public Employers[] GetEmployers()
        {
            List<Employers> employersList = new List<Employers>();

            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);

            string selectEmployersSQL = "SELECT [Id], [UserId], [FirstName],[SecondName],[Company] FROM [dbo].[Employers]";
            SqlCommand selectEmployersCommand = new SqlCommand(selectEmployersSQL, connection);

            connection.Open();
            using (var dataReader = selectEmployersCommand.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    Employers empl = new Employers(null);
                    empl.Id = (Guid)(dataReader["Id"]);
                    empl.UserId = (Guid)(dataReader["UserId"]);
                    empl.FirstName = dataReader["FirstName"].ToString();
                    empl.SecondName = dataReader["SecondName"].ToString();
                    empl.Company = dataReader["Company"].ToString();

                    //empl.User це призведе до повтору даних і тд???

                    employersList.Add(empl);
                }
            }

            connection.Close();

            return employersList.ToArray();
        }

        public void Save(Employers item)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            _dataSet.Tables["Employers"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Employers"].Columns[0] };
            item.Row = _dataSet.Tables["Employers"].Rows.Find(item.Id);
            if (item.Row == null)
            {
                item.Row = _dataSet.Tables["Employers"].NewRow();
                _dataSet.Tables["Employers"].Rows.Add(item.Row);
            }
            item.Row["Id"] = item.Id;
            item.Row["UserId"] = item.UserId;
            item.Row["FirstName"] = item.FirstName;
            item.Row["SecondName"] = item.SecondName;
            item.Row["Company"] = item.Company;

            GetEmployersDataAdapter().Update(_dataSet, "Employers");
        }

        public void Save(Employers[] employers)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            foreach (var item in employers)
            {
                ////var row = _dataSet.Tables["Product"].Rows.Find(product.ProductID);
                ////if (row != null)
                if (item.Row != null)
                {
                    ////if (item.IsDeleted)
                    ////{
                    ////    item.Row.Delete();
                    ////}
                    ////else
                    {
                        item.Row["Id"] = item.Id;
                        item.Row["UserId"] = item.UserId;
                        item.Row["FirstName"] = item.FirstName;
                        item.Row["SecondName"] = item.SecondName;
                        item.Row["Company"] = item.Company;
                    }
                }
                else
                {
                    _dataSet.Tables["Employers"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Employers"].Columns[0] };
                    item.Row = _dataSet.Tables["Employers"].Rows.Find(item.Id);
                    if (item.Row == null)
                    {
                        item.Row = _dataSet.Tables["Employers"].NewRow();
                        _dataSet.Tables["Employers"].Rows.Add(item.Row);
                    }

                    item.Row["Id"] = item.Id;
                    item.Row["UserId"] = item.UserId;
                    item.Row["FirstName"] = item.FirstName;
                    item.Row["SecondName"] = item.SecondName;
                    item.Row["Company"] = item.Company;
                }
            }

            GetEmployersDataAdapter().Update(_dataSet, "Employers");
        }

        public Employers[] GetEmployersByAdapter()
        {
            List<Employers> employersList = new List<Employers>();

            SqlDataAdapter emplsAdapter = GetEmployersDataAdapter();

            emplsAdapter.Fill(_dataSet, "Employers");

            foreach (DataRow row in _dataSet.Tables["Employers"].Rows)
            {
                Employers employer = new Employers(row);

                employer.Id = (Guid)(row["Id"]);
                employer.UserId = (Guid)(row["UserId"]);
                employer.FirstName = row["FirstName"].ToString();
                employer.SecondName = row["SecondName"].ToString();
                employer.Company = row["Company"].ToString();

                employersList.Add(employer);
            }

            return employersList.ToArray();
        }

        private SqlDataAdapter GetEmployersDataAdapter()
        {
            if (_employersAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectEmployersSQL = "SELECT * FROM [dbo].[Employers]";
                SqlCommand selectEmployersCommand = new SqlCommand(selectEmployersSQL, connection);

                _employersAdapter = new SqlDataAdapter(selectEmployersSQL, connection);

                CreateUpdateCommand(_employersAdapter);
            }
            return _employersAdapter;
        }

        private void CreateUpdateCommand(SqlDataAdapter adapter)
        {
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
            adapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            adapter.InsertCommand = commandBuilder.GetInsertCommand();
            ////adapter.DeleteCommand = commandBuilder.GetDeleteCommand();
        }

        private string GetConnectionString()
        {
            return @"Data Source=(localdb)\v11.0;Initial Catalog=MyDb;Integrated Security=True";
        }
    }
}
