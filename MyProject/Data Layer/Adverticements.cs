namespace MyProject.Data_Layer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data;
    using System.Data.Entity.Spatial;

    public partial class Adverticements
    {
        private DataRow _row;

        public DataRow Row
        {
            get { return _row; }
            set
            {
                if (_row == null)
                    _row = value;
            }
        }


        public Adverticements(DataRow row)
        {
            _row = row;
        }

        public Guid Id { get; set; }

        public Guid AuthorId { get; set; }

        public string Content { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime CreationDate { get; set; }

        [Required]
        [StringLength(30)]
        public string Company { get; set; }

        public int State { get; set; }
    }
}
