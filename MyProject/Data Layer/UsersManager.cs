﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProject.Data_Layer
{
    class UsersManager
    {
        private DataSet _dataSet = new DataSet();
        private SqlDataAdapter _usersAdapter;

        public Users[] GetUsers()
        {
            List<Users> usersList = new List<Users>();

            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);

            string selectUsersSQL = "SELECT [Id], [UserName], [Password],[Role],[Position],[ApprovingStatus] FROM [dbo].[Users]";
            SqlCommand selectUsersCommand = new SqlCommand(selectUsersSQL, connection);

            connection.Open();
            using (var dataReader = selectUsersCommand.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    Users user = new Users(null);
                    user.Id = (Guid)(dataReader["Id"]);
                    user.UserName = dataReader["UserName"].ToString();
                    user.Password = dataReader["Password"].ToString();
                    user.Role = Convert.ToInt32(dataReader["Role"]);
                    user.Position = Convert.ToInt32(dataReader["Position"]);
                    user.ApprovingStatus = Convert.ToInt32(dataReader["ApprovingStatus"]);

                    usersList.Add(user);
                }
            }

            connection.Close();

            return usersList.ToArray();
        }

        public void Save(Users item)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            _dataSet.Tables["Users"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Users"].Columns[0] };
            item.Row = _dataSet.Tables["Users"].Rows.Find(item.Id);
            if (item.Row == null)
            {
                item.Row = _dataSet.Tables["Users"].NewRow();
                _dataSet.Tables["Users"].Rows.Add(item.Row);
            }
            item.Row["UserName"] = item.UserName;
            item.Row["Password"] = item.Password;
            item.Row["Role"] = item.Role;
            item.Row["Position"] = item.Position;
            item.Row["ApprovingStatus"] = item.ApprovingStatus;

            GetUsersDataAdapter().Update(_dataSet, "Users");
        }

        public void Save(Users[] users)
        {
            if (_dataSet.Tables.Count == 0)
                return;

            foreach (var item in users)
            {
                ////var row = _dataSet.Tables["Product"].Rows.Find(product.ProductID);
                ////if (row != null)
                if (item.Row != null)
                {
                    ////if (item.IsDeleted)
                    ////{
                    ////    item.Row.Delete();
                    ////}
                    ////else
                    {
                        item.Row["UserName"] = item.UserName;
                        item.Row["Password"] = item.Password;
                        item.Row["Role"] = item.Role;
                        item.Row["Position"] = item.Position;
                        item.Row["ApprovingStatus"] = item.ApprovingStatus;
                    }
                }
                else
                {
                    _dataSet.Tables["Users"].PrimaryKey = new DataColumn[] { _dataSet.Tables["Users"].Columns[0] };
                    item.Row = _dataSet.Tables["Users"].Rows.Find(item.Id);
                    if (item.Row == null)
                    {
                        item.Row = _dataSet.Tables["Users"].NewRow();
                        _dataSet.Tables["Users"].Rows.Add(item.Row);
                    }

                    item.Row["UserName"] = item.UserName;
                    item.Row["Password"] = item.Password;
                    item.Row["Role"] = item.Role;
                    item.Row["Position"] = item.Position;
                    item.Row["ApprovingStatus"] = item.ApprovingStatus;
                }
            }

            GetUsersDataAdapter().Update(_dataSet, "Users");
        }
        
        public Users[] GetUsersByAdapter()
        {
            List<Users> usersList = new List<Users>();

            SqlDataAdapter usersAdapter = GetUsersDataAdapter();

            usersAdapter.Fill(_dataSet, "Users");

            foreach (DataRow row in _dataSet.Tables["Users"].Rows)
            {
                Users user = new Users(row);

                user.Id = (Guid)(row["Id"]);
                user.UserName = row["UserName"].ToString();
                user.Password = row["Password"].ToString();
                user.Role = Convert.ToInt32(row["Role"]);
                user.Position = Convert.ToInt32(row["Position"]);
                user.ApprovingStatus = Convert.ToInt32(row["ApprovingStatus"]);

                usersList.Add(user);
            }

            return usersList.ToArray();
        }

        private SqlDataAdapter GetUsersDataAdapter()
        {
            if (_usersAdapter == null)
            {
                string connectionString = GetConnectionString();
                SqlConnection connection = new SqlConnection(connectionString);

                string selectUsersSQL = "SELECT * FROM [dbo].[Users]";
                SqlCommand selectUsersCommand = new SqlCommand(selectUsersSQL, connection);

                _usersAdapter = new SqlDataAdapter(selectUsersSQL, connection);

                CreateUpdateCommand(_usersAdapter);
            }
            return _usersAdapter;
        }

        private void CreateUpdateCommand(SqlDataAdapter adapter)
        {
            SqlCommandBuilder commandBuilder = new SqlCommandBuilder(adapter);
            adapter.UpdateCommand = commandBuilder.GetUpdateCommand();
            adapter.InsertCommand = commandBuilder.GetInsertCommand();
            ////adapter.DeleteCommand = commandBuilder.GetDeleteCommand();
        }

        private string GetConnectionString()
        {
            return @"Data Source=(localdb)\v11.0;Initial Catalog=MyDb;Integrated Security=True";
        }
    }
}
