﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class EmployersUCVM:BaseVM, ICollectionsEntity
    {
        public EmployersUCVM(List<Employer> employers)
        {
            EmployerList = new ObservableCollection<EmployerVM>(employers.Select(b => new EmployerVM(b)));
        }

        private ObservableCollection<EmployerVM> _employerList = new ObservableCollection<EmployerVM>();
        public ObservableCollection<EmployerVM> EmployerList
        {
            get { return _employerList; }
            set { _employerList = value; OnPropertyChanged("EmployerList"); }
        }

        public Visibility EditButtonVisibility
        {
            get { return SelectedEmployer != null ? Visibility.Visible : Visibility.Collapsed; }
        }

        private EmployerVM _selectedEmployer;
        public EmployerVM SelectedEmployer
        {
            get { return _selectedEmployer; }
            set
            {
                _selectedEmployer = value;
                OnPropertyChanged("SelectedEmployer");
                OnPropertyChanged("EditButtonVisibility");

                OnPropertyChanged("EmployerList");

                OnPropertyChanged("FirstName");
                OnPropertyChanged("SecondName");
                OnPropertyChanged("CompanyName");
                OnPropertyChanged("EmployerFullName");
                OnPropertyChanged("State");
            }
        }

        public void Add(object item)
        {
            EmployerVM newItem = item as EmployerVM;
            if (newItem == null)
                return;

            _employerList.Add(newItem);
        }
    }
}
