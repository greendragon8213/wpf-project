﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class AdverticementVM: BaseVM
    {
        public Adverticement Adverticement;

        public AdverticementVM(Adverticement ad)
        {
            this.Adverticement = ad;
        }

        #region properties

        public string Content
        {
            get { return Adverticement.Content; }
            set { Adverticement.Content = value; OnPropertyChanged("Content"); }
        }

        public DateTime Date
        {
            get { return Adverticement.Date; }
        }

        public string Company
        {
            get { return Adverticement.Company; }
        }

        public State State
        {
            get { return Adverticement.State; }
            set { Adverticement.State = value; OnPropertyChanged("State"); }
        }

        public EmployerVM Author
        {
            get { return new EmployerVM(Adverticement.Author); }
        }


        #endregion
    }
}
