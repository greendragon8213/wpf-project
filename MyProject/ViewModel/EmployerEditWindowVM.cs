﻿using MyProject.Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class EmployerEditWindowVM: BaseVM
    {
        private EmployerVM _employer;
        private bool _isEdit;
        ICollectionsEntity _collectionEntity;

        private string _firstName;
        private ApprovingStatus _state;
        private string _secondName;
        private string _companyName;
        ////private string _employerFullName;

        public EmployerEditWindowVM(ICollectionsEntity collectionEntity, EmployerVM empl)
        {
            _employer = empl;
            _isEdit = _employer != null;

            if (_employer == null)
                _employer = new EmployerVM(new Employer());

            _collectionEntity = collectionEntity;

            InitilizeData();
        }

        private bool _OkCancelButtonEnabled;
        public bool OkCancelButtonEnabled
        {
            get { return _OkCancelButtonEnabled; }
        }

        internal void ButtonOKClick()
        {
            CommitData();

            if (!_isEdit)
                _collectionEntity.Add(_employer);
        }

        #region properties
        

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; OnPropertyChanged("FirstName"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled");}
        }

        public string CompanyName
        {
            get { return _companyName; }
            set 
            {
                _companyName = value; OnPropertyChanged("CompanyName");
            _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled");
            }
        }

        public ApprovingStatus State
        {
            get { return _state; }
            set 
            { 
                _state = value; OnPropertyChanged("State");
                _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled");
            }
        }

        public string SecondName
        {
            get { return _secondName; }
            set 
            { 
                _secondName = value; OnPropertyChanged("SecondName");
                _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled");
            }
        }

        private void InitilizeData()
        {
            _firstName = _employer.FirstName;
            _companyName = _employer.CompanyName;
            _state = _employer.State;
            _secondName = _employer.SecondName;
        }
        #endregion

        private void CommitData()
        {
            _employer.FirstName = _firstName;
            _employer.CompanyName = _companyName;
            _employer.State = _state;
            _employer.SecondName = _secondName;

            EmployersManager.CommitDataToDB(_employer.Employer);
        }

        public Guid EmployerId 
        {
            get { return _employer.Employer.Id; }
        }
    }
}
