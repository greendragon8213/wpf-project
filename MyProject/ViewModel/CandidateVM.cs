﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    //Просто обгортка
    public class CandidateVM: BaseVM
    {
        public Candidate Candidate;

        public CandidateVM(Candidate cand)
        {
            this.Candidate = cand;
        }

        #region properties
        public string Location
        {
            get { return Candidate.Location; }
            set { Candidate.Location = value; OnPropertyChanged("FirstName"); }
        }

        public ApprovingStatus State
        {
            get { return Candidate.State; }
            set { Candidate.State = value; OnPropertyChanged("State"); }
        }

        public string Skills
        {
            get { return Candidate.Skills; }
            set { Candidate.Skills = value; OnPropertyChanged("Skills"); }
        }

        public int? Salary
        {
            get { return Candidate.Salary; }
            set { Candidate.Salary = value; OnPropertyChanged("Salary"); }
        }

        public string Experience
        {
            get { return Candidate.Experience; }
            set { Candidate.Experience = value; OnPropertyChanged("Experience"); }
        }
        #endregion
    }
}
