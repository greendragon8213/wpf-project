﻿using MyProject.Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class AdverticementEditWindowVM: BaseVM
    {
        private AdverticementVM _adverticement;
        private bool _isEdit;
        ICollectionsEntity _collectionEntity;

        private string _content;
        private State _state;

        public AdverticementEditWindowVM(ICollectionsEntity collectionEntity, AdverticementVM adv)
        {
            _adverticement = adv;
            _isEdit = _adverticement != null;

            ////if (_adverticement == null)
            ////    _adverticement = new AdverticementVM(new Adverticement());

            _collectionEntity = collectionEntity;

            InitilizeData();
        }

        private bool _OkCancelButtonEnabled;
        public bool OkCancelButtonEnabled
        {
            get { return _OkCancelButtonEnabled; }
        }

        #region properties

        public string Content
        {
            get { return _content; }
            set 
            { 
                _content = value; OnPropertyChanged("Content");
                _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled");
            }
        }

        public DateTime Date
        {
            get { return _adverticement.Date; }
        }

        public string Company
        {
            get { return _adverticement.Company; }
        }

        public State State
        {
            get { return _state; }
            set 
            { 
                _state = value; 
                OnPropertyChanged("State");
                _OkCancelButtonEnabled = true;
                OnPropertyChanged("OkCancelButtonEnabled");
            }
        }

        public EmployerVM Author
        {
            get { return _adverticement.Author; OnPropertyChanged("Author"); }
        }

        #endregion

        internal void ButtonOKClick()
        {
            CommitData();

            if (!_isEdit)
                _collectionEntity.Add(_adverticement);
        }

         private void InitilizeData()
        {
            _content = _adverticement.Content;
            _state = _adverticement.State;
        }

        private void CommitData()
        {
            _adverticement.Content = _content;
            _adverticement.State = _state;
            AdverticementsManager.CommitDataToDB(_adverticement.Adverticement);
        }
    }
}
