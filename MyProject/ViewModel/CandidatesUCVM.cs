﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    class CandidatesUCVM: BaseVM, ICollectionsEntity
    {
        public CandidatesUCVM(List<Candidate> candidates)
        {
            CandidateList = new ObservableCollection<CandidateVM>(candidates.Select(b => new CandidateVM(b)));
        }

        private ObservableCollection<CandidateVM> _candidateList = new ObservableCollection<CandidateVM>();
        public ObservableCollection<CandidateVM> CandidateList
        {
            get { return _candidateList; }
            set { _candidateList = value; OnPropertyChanged("CandidateList");}
        }

        public Visibility EditButtonVisibility
        {
            get { return SelectedCandidate != null ? Visibility.Visible : Visibility.Collapsed; }
        }

        private CandidateVM _selectedCandidate;
        public CandidateVM SelectedCandidate
        {
            get { return _selectedCandidate; }
            set
            {
                _selectedCandidate = value;
                OnPropertyChanged("SelectedCandidate");
                OnPropertyChanged("EditButtonVisibility");

                OnPropertyChanged("CandidateList");
                
                OnPropertyChanged("Experience");
                OnPropertyChanged("Skills");
                OnPropertyChanged("Salary");
                OnPropertyChanged("FirstName");
                OnPropertyChanged("State");
            }
        }

        public void Add(object item)
        {
            CandidateVM newItem = item as CandidateVM;
            if (newItem == null)
                return;

            _candidateList.Add(newItem);
        }
    }
}
