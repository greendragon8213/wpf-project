﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class AdverticementsUCVM: BaseVM, ICollectionsEntity
    {
        public AdverticementsUCVM(List<Adverticement> ads)
        {
            AdverticementList = new ObservableCollection<AdverticementVM>(ads.Select(b => new AdverticementVM(b)));
        }

        private ObservableCollection<AdverticementVM> _adverticementList = new ObservableCollection<AdverticementVM>();
        public ObservableCollection<AdverticementVM> AdverticementList
        {
            get { return _adverticementList; }
            set { _adverticementList = value; OnPropertyChanged("AdverticementList"); }
        }

        public Visibility EditButtonVisibility
        {
            get { return SelectedAdverticement != null ? Visibility.Visible : Visibility.Collapsed; }
        }

        private AdverticementVM _selectedAdverticement;
        public AdverticementVM SelectedAdverticement
        {
            get { return _selectedAdverticement; }
            set
            {
                _selectedAdverticement = value;
                OnPropertyChanged("SelectedAdverticement");
                OnPropertyChanged("EditButtonVisibility");

                OnPropertyChanged("AdverticementList");

                OnPropertyChanged("Content");
                OnPropertyChanged("State");
            }
        }

        public void Add(object item)
        {
            AdverticementVM newItem = item as AdverticementVM;
            if (newItem == null)
                return;

            _adverticementList.Add(newItem);
        }
    }
}
