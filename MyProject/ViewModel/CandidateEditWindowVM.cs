﻿using MyProject.Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class CandidateEditWindowVM: BaseVM
    {
        private CandidateVM _candidate;
        private bool _isEdit;
        ICollectionsEntity _collectionEntity;

        private string _location;
        private ApprovingStatus _state;
        private int? _salary;
        private string _skills;
        private string _experience;

        public CandidateEditWindowVM(ICollectionsEntity collectionEntity, CandidateVM cand)
        {
            _candidate = cand;
            _isEdit = _candidate != null;

            if (_candidate == null)
                _candidate = new CandidateVM(new Candidate());

            _collectionEntity = collectionEntity;

            InitilizeData();
        }

        private bool _OkCancelButtonEnabled;
        public bool OkCancelButtonEnabled
        {
            get { return _OkCancelButtonEnabled; }
        }

        internal void ButtonOKClick()
        {
            CommitData();

            if (!_isEdit)
                _collectionEntity.Add(_candidate);
        }

        #region properties       

        public string Location
        {
            get { return _location; }
            set { _location = value; OnPropertyChanged("location"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled"); }
        }

        public string Experience
        {
            get { return _experience; }
            set { _experience = value; OnPropertyChanged("Experience"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled"); }
        }

        public string Skills
        {
            get { return _skills; }
            set { _skills = value; OnPropertyChanged("Skills"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled"); }
        }

        public ApprovingStatus State
        {
            get { return _state; }
            set { _state = value; OnPropertyChanged("State"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled"); }
        }

        public int? Salary
        {
            get { return _salary; }
            set { _salary = value; OnPropertyChanged("Salary"); _OkCancelButtonEnabled = true; OnPropertyChanged("OkCancelButtonEnabled"); }
        }

        private void InitilizeData()
        {
            _location = _candidate.Location;
            _experience = _candidate.Experience;
            _skills = _candidate.Skills;
            _state = _candidate.State;
            _salary = _candidate.Salary;
        }
        #endregion

        private void CommitData()
        {
            _candidate.Location = _location;
            _candidate.Experience = _experience;
            _candidate.Skills = _skills;
            _candidate.State = _state;
            _candidate.Salary = _salary;

            CandidatesManager.CommitDataToDB(_candidate.Candidate);
        }

        
    }
}
