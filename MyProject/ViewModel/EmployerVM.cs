﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkSearch.classes;

namespace MyProject.ViewModel
{
    public class EmployerVM : BaseVM
    {
        public Employer Employer;

        public EmployerVM(Employer emp)
        {
            this.Employer = emp;
        }        

        #region properties

        public int AdsCount
        {
            get { return Employer.Ads.Count; }
        }

        public ApprovingStatus State
        {
            get { return Employer.State; }
            set { Employer.State = value; OnPropertyChanged("State"); }
        }

        public string FirstName
        {
            get { return Employer.FirstName; }
            set { Employer.FirstName = value; OnPropertyChanged("FirstName"); }
        }

        public string SecondName
        {
            get { return Employer.SecondName; }
            set { Employer.SecondName = value; OnPropertyChanged("SecondName"); OnPropertyChanged("EmployerFullName"); }
        }

        public string CompanyName
        {
            get { return Employer.CompanyName; }
            set { Employer.CompanyName = value; OnPropertyChanged("CompanyName"); OnPropertyChanged("EmployerFullName"); }
        }

        public string EmployerFullName
        {
            get { return Employer.EmployerFullName; }
        }

        #endregion

    }
}
